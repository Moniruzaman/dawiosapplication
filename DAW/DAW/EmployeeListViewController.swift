//
//  EmployeeListViewController.swift
//  DAW
//
//  Created by SM Moniruzaman on 12/17/18.
//

import UIKit

class EmployeeListViewController: UIViewController , UITableViewDelegate, UITableViewDataSource {
    

    @IBOutlet weak var tableView: UITableView!
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 10
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let tableCell = self.tableView.dequeueReusableCell(withIdentifier: "customemployeecell" , for: indexPath) as! EmployeeTableViewCell
        tableCell.employeeName.text = "SM Moniruzaman"
        tableCell.employeeDesignation.text = "Software Engineer"
        tableCell.employeedepartment.text = "Energov"
        
        return tableCell
        
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        
        
        // Do any additional setup after loading the view.
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
