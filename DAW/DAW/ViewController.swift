//
//  ViewController.swift
//  DAW
//
//  Created by SM Moniruzaman on 12/10/18.
//

import UIKit

class ViewController: UIViewController {

  
    @IBOutlet weak var username: UITextField!
    @IBOutlet weak var password: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }

    @IBAction func SignInAction(_ sender: Any) {
        
        if (username.text?.caseInsensitiveCompare("SM") == ComparisonResult.orderedSame && password.text?.caseInsensitiveCompare("password") == ComparisonResult.orderedSame )
        {
            let alert = UIAlertController(title: "Success!", message: "Login Successful", preferredStyle: .alert)
            
            self.present(alert, animated: true, completion: nil)
        }
           
    }
    
}

